(($) => {
    'use strict';

    // same-height
    const getSameHeight = $('#get-same-height'),
        setSameHeight = $('#set-same-height');


    $(window).on('load', () => {
        let sHeight = getSameHeight.innerHeight();
        setSameHeight.height(sHeight);
    });

    $('.card.moviment').on('mouseover', () => {
        $(this).addClass('active');
    }).on('mouseout', () => {
        $(this).removeClass('active');
    })



    let postPaginate = $('.single .post-paginate'),
        postPagination = $('.single .navigation');

    postPaginate.find('.post-page-numbers').each((i, item) => {
        $(item).addClass('page-item-' + $(item).text());
    });

    $(window).on('resize load', () => {
        let winsize = $(window).innerWidth();

        if (winsize <= 992) {
            $('#primary').addClass('container-fluid').removeClass('container');
        } else {
            $('#primary').removeClass('container-fluid').addClass('container');
        }
    });

    if (postPaginate.find('a').length > 0) {
        let current = postPaginate.find('.current');

        // adiciona botão para o próximo post
        if (postPaginate.find('.post-page-numbers').last().hasClass('current')) {
            let prev = postPaginate.find('.page-item-' + (parseInt(current.text()) - 1)),
                prevLink = $('<a />').attr('href', prev.attr('href')).addClass('prev'),
                nextLink = $('<a />').attr('href', $('link[rel=next]').attr('href')).addClass('next');

            postPagination.append(prevLink.text('Página Anterior'), nextLink.text('Próxima Postagem'));
        } else if (postPaginate.find('.post-page-numbers').first().hasClass('current')) {
            let next = postPaginate.find('.page-item-' + (parseInt(current.text()) + 1)),
                prevLink = $('<a />').attr('href', $('link[rel=prev]').attr('href')).addClass('prev'),
                nextLink = $('<a />').attr('href', next.attr('href')).addClass('next');

            postPagination.append(prevLink.text('Postagem Anterior'), nextLink.text('Próxima Página'));

        } else {
            let prev = postPaginate.find('.page-item-' + (parseInt(current.text()) - 1)),
                next = postPaginate.find('.page-item-' + (parseInt(current.text()) + 1)),
                prevLink = $('<a />').attr('href', prev.attr('href')).addClass('prev'),
                nextLink = $('<a />').attr('href', next.attr('href')).addClass('next');

            postPagination.append(prevLink.text('Página Anterior'), nextLink.text('Próxima Página'));
        }
    } else {
        let nextHref = $('link[rel=next]').attr('href'),
            prevHref = $('link[rel=prev]').attr('href');

        let prevLink = $('<a />').attr('href', prevHref).addClass('prev'),
            nextLink = $('<a />').attr('href', nextHref).addClass('next');
        
        postPagination.append(prevLink.text('Postagem Anterior'));

        if (typeof nextHref !== 'undefined' && nextHref.length > 0) {
            postPagination.append(nextLink.text('Próxima Postagem'));
        }
    }



    $(window).on('resize load', (e) => {
        let size = $(this).innerWidth(),
            images = $('.ads-image');

        e.stopPropagation();


        if (size <= 992) {
            images.each((i, img) => {
                let current = $(img);
                current.attr('src', 'https://via.placeholder.com/'+ parseInt(current.parents('.pub-block').innerWidth() - 30) +'x250?text=Publicidade');
            })
        }
    });
})(jQuery);