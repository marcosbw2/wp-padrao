<article class="post-item row d-flex align-items-center my-5">
    <figure class="post-thumbnail figure col-sm-4 my-0">
        <a href="<?php echo get_permalink(); ?>"><img class="img-fluid" src="<?php the_post_thumbnail_url('sonar-thumbnail'); ?>" alt="<?php the_post_thumbnail_caption(); ?>"></a>
    </figure>
    <div class="post-content py-4 col-sm-8">
        <header>
            <small><strong><?php printf(get_the_category_list(', ')); ?></strong></small>
            <h4><strong><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></strong></h4>
        </header>

        <p class="my-0"><?php printf(sonar_post_excerpt(150, get_the_excerpt(), get_permalink())); ?></p>

        <div class="post-goto mt-3">
            <a href="<?php printf(get_permalink()); ?>" class="btn btn-primary">Continuar lendo</a>
        </div>
    </div>
</article>