<article class="post-item post-item-feature row my-5">
    <figure class="post-thumbnail figure col-12 my-0">
        <a href="<?php echo get_permalink(); ?>"><img class="img-fluid" src="<?php the_post_thumbnail_url('large'); ?>" alt="<?php the_post_thumbnail_caption(); ?>"></a>
        <span><?php printf(get_the_category_list(', ')); ?></span>
    </figure>
    <div class="post-content py-4 col-12 d-flex justify-content-center">
        <h2><strong><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></strong></h2>
    </div>
</article>