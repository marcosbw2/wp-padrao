<div class="row">
    <div class="col d-flex justify-content-center pt-5">
        <div class="navigation">
            <?php $nextLink = get_next_posts_link( 'Próxima Página' ); ?>
            <?php $previousLink = get_previous_posts_link( 'Página Anterior' ); ?>

            <p><?php echo sonar_navigator($nextLink, $previousLink); ?></p>
        </div>
    </div>
</div>