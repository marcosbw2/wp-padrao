<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>

    <section id="primary" class="container">
        <?php get_template_part('parts/full-ads'); ?>
        <div class="row">
            <main id="main" class="site-main col col-md-8">
                <?php
                    if ( have_posts() ) {
                        $index = 0;
                        while (have_posts()) {
                            the_post();
                            $index++;
                            if ($index === 1) {
                                get_template_part('parts/post-list-feature');
                            } else {
                                get_template_part('parts/post-list');
                            }

                            if ($index % 3 === 1 && $index > 2) {
                                get_template_part('parts/post-ads');
                            }
                        }
                    }

                    get_template_part('parts/navigation');
                ?>
            </main><!-- .site-main -->
            <?php get_sidebar(); ?>
        </div>
    </section><!-- .content-area -->

<?php
get_footer();
