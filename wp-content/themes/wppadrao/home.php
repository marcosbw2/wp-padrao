<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();

?>
<section class="page-home">
    <div class="container">
        <div class="row mt-5 d-flex align-items-stretch">
            <div class="col-sm-6">
                <div id="set-same-height" class="card">
                    <img src="https://picsum.photos/1280/768/?random" class="card-img-top" alt="...">
                    <div class="card-body d-flex align-items-center">
                        <p class="card-text" style="font-size: 35px; background-color:	#808080;">Lorem ipsum dolor sit amet, consectetur adipiscing.</p>
                    </div>
                </div>
            </div>
            <div id="get-same-height" class="col-sm-6">
                <ul class="list-group feature-list">
                    <li class="list-group-item">
                        <small class="text-muted">3 days ago</small>
                        <h5 class="mb-1">Lorem ipsum dolor sit amet, consectetur adipiscing.</h5>
                    </li>
                    <li class="list-group-item">
                        <small class="text-muted">3 days ago</small>
                        <h5 class="mb-1">Lorem ipsum dolor sit amet, consectetur adipiscing.</h5>
                    </li>
                    <li class="list-group-item">
                        <small class="text-muted">3 days ago</small>
                        <h5 class="mb-1">Lorem ipsum dolor sit amet, consectetur adipiscing.</h5>
                    </li>
                </ul>
                <div class="row">
                    <div class="col">
                        <div class="media digital-edition p-3 d-flex align-items-center">
                            <img class="mr-3" src="https://picsum.photos/110/150/?random" alt="Generic placeholder image">
                            <div class="media-body">
                                <h5 class="mt-2">Media heading</h5>
                                <p>Aqui é uma breve descrição a imagem a esquerda!</p>
                                <button class="media-botton btn btn-danger">Acessar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="colunistas">
            <div class="row mt-5">
                <div class="col colunistas-header">
                    <div class="title-border d-flex justify-content-between">
                        <h4 class="">Colunistas</h4>
                        <a href="#">Ver todos</a>
                    </div>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-sm-3 d-flex flex-row">
                    <div class="relative">
                        <p>Relacionados</p>
                    </div>
                    <div class="media">
                        <img class="mr-3" src="https://picsum.photos/64/64/?random" alt="Generic placeholder image">
                        <div class="media-body">
                            <h5 class="mt-0 style: font-size = small">Nome do Colunista</h5>
                            Lorem ipsum dolor sit amet, consectetur adipiscing.
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 d-flex flex-row">
                    <div class="media">
                        <img class="mr-3" src="https://picsum.photos/64/64/?random" alt="Generic placeholder image">
                        <div class="media-body">
                            <h5 class="mt-0 style: font-size = small">Nome do Colunista</h5>
                            Lorem ipsum dolor sit amet, consectetur adipiscing.
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 d-flex flex-row">
                    <div class="media">
                        <img class="mr-3" src="https://picsum.photos/64/64/?random" alt="Generic placeholder image">
                        <div class="media-body">
                            <h5 class="mt-0 style: font-size = small">Nome do Colunista</h5>
                            Lorem ipsum dolor sit amet, consectetur adipiscing.
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 d-flex flex-row">
                    <div class="media">
                        <img class="mr-3" src="https://picsum.photos/64/64/?random" alt="Generic placeholder image">
                        <div class="media-body">
                            <h5 class="mt-0 style: font-size = small">Nome do Colunista</h5>
                            Lorem ipsum dolor sit amet, consectetur adipiscing.
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-media mt-5">
                <div class="row">
                    <div class="col-md-9">
                        <div class="row mb-4">
                            <div class="col colunistas-header d-flex justify-content-between">
                                <h4 class="pb-2 title-border">Ultimas Noticias</h4>
                            </div>
                        </div>
                        <ul class="media-group pl-0">
                            <li>
                                <div class="media">
                                    <img class="mr-4" src="https://picsum.photos/190/130/?random" alt="Generic placeholder image">
                                    <div class="media-body">
                                        <h5 class="mt-2 style: font-size = small">Nome do Colunista</h5>
                                        <h5>Lorem ipsum dolor sit amet, consectetur adipiscing.</h5>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="media">
                                    <img class="mr-4 mt-3" src="https://picsum.photos/190/130/?random" alt="Generic placeholder image">
                                    <div class="media-body">
                                        <h5 class="mt-4 style: font-size = small">Nome do Colunista</h5>
                                        <h5>Lorem ipsum dolor sit amet, consectetur adipiscing.</h5>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="media">
                                    <img class="mr-4 mt-3" src="https://picsum.photos/190/130/?random" alt="Generic placeholder image">
                                    <div class="media-body">
                                        <h5 class="mt-4 style: font-size = small">Nome do Colunista</h5>
                                        <h5>Lorem ipsum dolor sit amet, consectetur adipiscing.</h5>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="media">
                                    <img class="mr-4 mt-3" src="https://picsum.photos/190/130/?random" alt="Generic placeholder image">
                                    <div class="media-body">
                                        <h5 class="mt-4 style: font-size = small">Nome do Colunista</h5>
                                        <h5>Lorem ipsum dolor sit amet, consectetur adipiscing.</h5>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="media">
                                    <img class="mr-4 mt-3" src="https://picsum.photos/190/130/?random" alt="Generic placeholder image">
                                    <div class="media-body">
                                        <h5 class="mt-4 style: font-size = small">Nome do Colunista</h5>
                                        <h5>Lorem ipsum dolor sit amet, consectetur adipiscing.</h5>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <div class="border-media feature p-4">
                            <H5>MAIS LIDAS</H5>
                            <ul class="media-group2 p-0 mb-0">

                                <li>
                                    <div class="media">
                                        <div class="media-body">
                                            <h6 class="font-size: small">Lorem ipsum dolor sit amet, consectetur adipiscing.</h6>
                                        </div>
                                        <img class="ml-3" src="https://picsum.photos/64/64/?random" alt="Generic placeholder image">
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                        <div class="media-body">
                                            <h6 class="font-size: small">Lorem ipsum dolor sit amet, consectetur adipiscing.</h6>
                                        </div>
                                        <img class="ml-3" src="https://picsum.photos/64/64/?random" alt="Generic placeholder image">
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                        <div class="media-body">
                                            <h6 class="font-size: small">Lorem ipsum dolor sit amet, consectetur adipiscing.</h6>
                                        </div>
                                        <img class="ml-3" src="https://picsum.photos/64/64/?random" alt="Generic placeholder image">
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                        <div class="media-body">
                                            <h6 class="font-size: small">Lorem ipsum dolor sit amet, consectetur adipiscing.</h6>
                                        </div>
                                        <img class="ml-3" src="https://picsum.photos/64/64/?random" alt="Generic placeholder image">
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                        <div class="media-body">
                                            <h6 class="font-size: small">Lorem ipsum dolor sit amet, consectetur adipiscing.</h6>
                                        </div>
                                        <img class="ml-3" src="https://picsum.photos/64/64/?random" alt="Generic placeholder image">
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>


</section>

<?php
get_footer();
