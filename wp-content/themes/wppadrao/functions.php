<?php


if (!function_exists('sonar_theme_setup')) {
    function sonar_theme_setup()
    {
        // theme thumbnails
        add_theme_support( 'post-thumbnails' );
        add_image_size( 'sonar-thumbnail', 450, 450, true );
    }

    add_action( 'after_setup_theme', 'sonar_theme_setup' );
}

if (!function_exists('sonar_theme_scripts')) {
    function sonar_theme_scripts()
    {
        wp_enqueue_style( 'sonar-style', get_stylesheet_uri(), [] );

        wp_enqueue_script( 'sonar-scripts', get_theme_file_uri( '/assets/js/app.js' ), [], '1', true );

    }

    add_action( 'wp_enqueue_scripts', 'sonar_theme_scripts' );
}

if (!function_exists('sonar_excerpt_length')) {
    function sonar_excerpt_length( $length )
    {
        return 35;
    }

    add_filter( 'excerpt_length', 'sonar_excerpt_length', 999 );
}

if (!function_exists('sonar_post_excerpt')) {
    function sonar_post_excerpt($limit, $string, $link = null)
    {
        // strip tags to avoid breaking any html
        $string = strip_tags($string);
        if (strlen($string) > $limit) {

            // truncate string
            $stringCut = substr($string, 0, $limit);
            $endPoint = strrpos($stringCut, ' ');

            //if the string doesn't contain any space then it will cut without word basis.
            $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);

            if (isset($link)) {
                $string .= '...';
            } else {
                $string .= '...';
            }
        }

        return $string;
    }
}

if (!function_exists('sonar_navigator')) {
    function sonar_navigator($next, $prev)
    {
        $next = str_replace(['href'], ['class="next" href'], $next);
        $prev = str_replace(['href'], ['class="prev" href'], $prev);

        return sprintf('%s%s', $prev, $next);
    }
}

/**
 * The formatted output of a list of pages.
 *
 * Displays page links for paginated posts (i.e. includes the "nextpage".
 * Quicktag one or more times). This tag must be within The Loop.
 *
 * The defaults for overwriting are:
 * 'next_or_number' - Default is 'number' (string). Indicates whether page
 *      numbers should be used. Valid values are number and next.
 * 'nextpagelink' - Default is 'Next Page' (string). Text for link to next page.
 *      of the bookmark.
 * 'previouspagelink' - Default is 'Previous Page' (string). Text for link to
 *      previous page, if available.
 * 'pagelink' - Default is '%' (String).Format string for page numbers. The % in
 *      the parameter string will be replaced with the page number, so Page %
 *      generates "Page 1", "Page 2", etc. Defaults to %, just the page number.
 * 'before' - Default is '<p id="post-pagination"> Pages:' (string). The html
 *      or text to prepend to each bookmarks.
 * 'after' - Default is '</p>' (string). The html or text to append to each
 *      bookmarks.
 * 'text_before' - Default is '' (string). The text to prepend to each Pages link
 *      inside the <a> tag. Also prepended to the current item, which is not linked.
 * 'text_after' - Default is '' (string). The text to append to each Pages link
 *      inside the <a> tag. Also appended to the current item, which is not linked.
 *
 * @param string|array $args Optional. Overwrite the defaults.
 * @return string Formatted output in HTML.
 */
function custom_wp_link_pages( $args = '' ) {
    $defaults = array(
        'before' => '<p id="post-pagination">' . __( 'Pages:' ),
        'after' => '</p>',
        'text_before' => '',
        'text_after' => '',
        'next_or_number' => 'number',
        'nextpagelink' => __( 'Next page' ),
        'previouspagelink' => __( 'Previous page' ),
        'pagelink' => '%',
        'echo' => 1
    );

    $r = wp_parse_args( $args, $defaults );
    $r = apply_filters( 'wp_link_pages_args', $r );
    extract( $r, EXTR_SKIP );

    global $page, $numpages, $multipage, $more, $pagenow;

    $output = '';
    if ( $multipage ) {
        if ( 'number' == $next_or_number ) {
        } else {
            if ( $more ) {
                $output .= $before;
                $i = $page - 1;
                if ( $i && $more ) {
                    $output .= _wp_link_page( $i );
                    $output .= $previouspagelink . $text_after . '</a>';
                }
                $i = $page + 1;
                if ( $i <= $numpages && $more ) {
                    $output .= _wp_link_page( $i );
                    $output .= $nextpagelink . $text_after . '</a>';
                }
                $output .= $after;
            }
        }
    }

    if ( $echo )
        echo $output;

    return $output;
}