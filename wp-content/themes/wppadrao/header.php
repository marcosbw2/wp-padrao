<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="profile" href="https://gmpg.org/xfn/11" />
    <title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
    <?php wp_head(); ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
    <header class="main-header py-3">
        <div class="container">
            <div class="row">
                <nav class="main-navbar navbar navbar-light py-3">
                    <div class="col-sm-4 open-menu d-flex justify-content-left">
                        <button id="menu-slide"><i class="fas fa-bars"></i> Menu</button>
                    </div>

                    <div class="col-sm-4 text-center">
                        <a class="navbar-brand mr-0" href="/">LOGO</a>
                    </div>

                    <div class="col-sm-4 d-flex justify-content-end">
                        <form class="form-horizontal my-0 header-form-search" action="/" method="get">
                            <div class="form-group my-0 d-flex">
                                <input type="text" name="s" id="search" class="form-control" placeholder="Pesquisar" value="<?php the_search_query(); ?>" />
                                <button type="submit" class="btn btn-default ml-3"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </div>
                </nav>
            </div>
        </div>
    </header>


    <!--<div id="menu-slide" class="menu-slide">
        <h4 class="d-flex justify-content-between align-items-center">Menu <button>fechar <i class="fas fa-times"></i></button></h4>
        <span class="menu-line my-4"></span>
        <ul class="pl-0">
            <li><a href="#" class="d-flex justify-content-between">Segurança publica <span>></span></a></li>
            <li><a href="#" class="d-flex justify-content-between">Esportes <span>></span></a></li>
            <li><a href="#" class="d-flex justify-content-between">Geral <span>></span></a></li>
            <li><a href="#" class="d-flex justify-content-between">Economia <span>></span></a></li>
            <li><a href="#" class="d-flex justify-content-between">Colunistas <span>></span></a></li>
            <li><a href="#" class="d-flex justify-content-between">Colunistas <span>></span></a></li>
        </ul>
        <span class="menu-line my-4"></span>
        <ul class="pl-0">
            <li><a href="#" class="d-flex justify-content-between">Jornal Digital<span>></span></a></li>
        </ul>
    </div>-->

    <div id="content" class="site-content">
