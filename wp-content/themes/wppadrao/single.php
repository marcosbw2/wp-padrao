<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();

the_post();
?>

<main>
    <div class="container">
        <div class="row my-5">
            <div class="col page-header title">
                <h1 style="text-align: center"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing.</strong></h1>
                <div class="tag">Lorem ipsum dolor sit amet, consectetur adipiscing.</div>
            </div>
        </div>
        <div class="row">
            <article class="col-sm-8">
                <!-- conteudo do post/pagina -->
                <div class="row mb-3">
                    <div class="col post-header">
                        <span class="badge badge-secondary category-list"><?php echo get_the_category_list(', '); ?></span>
                        <span class="post-attributes d-flex flex-column"><?php echo get_post_time('d/m/Y H\hi', true); ?> Atualizado há menos de 1 minuto</span>
                    </div>
                </div>
                <div class="row">
                    <div class="card-text-center" style="width: 130rem;">
                        <img class="card-img-top" src="https://picsum.photos/1920/1080/?random" alt="Card image cap">
                        <div class="card-text-center">
                            <p class="text image"><small>Some quick example text to build on the card title and make up the bulk of the card's content.</small></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="post-content pt-5 col-12">
                        <div class="entry-content">
                            <?php the_content(); ?>
                        </div>
                        <p>
                            lorem ipsum dolor sit amet consectetur adipiscing elit maecenas dis mauris platea senectus cursus sem rhoncus nec habitant velit netus nostra posuere accumsan habitasse mus mollis turpis imperdiet aenean mattis morbi blandit volutpat justo scelerisque inceptos egestas neque dignissim taciti sed vehicula efficitur augue duis ornare magnis lobortis mi facilisi euismod per urna semper auctor class quis bibendum a tristique ante leo id vestibulum elementum tortor purus faucibus sapien erat nibh diam vivamus montes nunc fringilla phasellus suscipit odio commodo aliquet eleifend placerat facilisis magna luctus curabitur integer arcu quam ullamcorper molestie est conubia potenti pretium primis parturient etiam massa
                        </p>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col colunistas-header">
                        <div class="title-border d-flex justify-content-between">
                        </div>
                    </div>
                </div>

                <div class="row my-5">
                    <div class="col ">
                        <p class="share-items">Compartilhar
                            <a href="#" class="share-fb"><i class="fab fa-facebook-f"></i></a>
                            <a href="#" class="share-twitter"><i class="fab fa-twitter"></i></a>
                            <a href="#" class="share-whatsapp"><i class="fab fa-whatsapp"></i></a>
                        </p>
                        <div class="media">
                            <img class="mr-3" src="https://picsum.photos/64/64/?random" alt="Generic placeholder image">
                            <div class="media-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing.</p>

                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <aside class="col-sm-4">
                <div class="border-media feature p-4">
                    <H5>MAIS LIDAS</H5>
                    <ul class="media-group2 p-0 mb-0">
                        <li>
                            <div class="media">
                                <div class="media-body">
                                    <h6 class="small"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing.</strong></h6>
                                </div>
                                <img class="ml-3" src="https://picsum.photos/64/64/?random" alt="Generic placeholder image">
                            </div>
                        </li>
                        <li>
                            <div class="media">
                                <div class="media-body">
                                <h6 class="small"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing.</strong></h6>
                                </div>
                                <img class="ml-3" src="https://picsum.photos/64/64/?random" alt="Generic placeholder image">
                            </div>
                        </li>
                        <li>
                            <div class="media">
                                <div class="media-body">
                                <h6 class="small"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing.</strong></h6>
                                </div>
                                <img class="ml-3" src="https://picsum.photos/64/64/?random" alt="Generic placeholder image">
                            </div>
                        </li>
                        <li>
                            <div class="media">
                                <div class="media-body">
                                <h6 class="small"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing.</strong></h6>
                                </div>
                                <img class="ml-3" src="https://picsum.photos/64/64/?random" alt="Generic placeholder image">
                            </div>
                        </li>
                        <li>
                            <div class="media">
                                <div class="media-body">
                                <h6 class="small"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing.</strong></h6>
                                </div>
                                <img class="ml-3" src="https://picsum.photos/64/64/?random" alt="Generic placeholder image">
                            </div>
                        </li>
                    </ul>
                </div>
            </aside>
        </div>
        <div class="row">
            <section class="page-attributes posts-reslated col">
                <div class="row mt-4">
                    <div class="col">
                        <h3>Relacionados</h3>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-sm-6 col-md-3 mb-5">
                        <div class="card moviment">
                            <img src="https://picsum.photos/350/220/?random" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p>Fancy display heading</p>
                                <small class="text-muted">With faded secondary text</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 mb-5">
                        <div class="card moviment">
                            <img src="https://picsum.photos/350/220/?random" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p>Fancy display heading</p>
                                <small class="text-muted">With faded secondary text</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 mb-5">
                        <div class="card moviment">
                            <img src="https://picsum.photos/350/220/?random" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p>Fancy display heading</p>
                                <small class="text-muted">With faded secondary text</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 mb-5">
                        <div class="card moviment">
                            <img src="https://picsum.photos/350/220/?random" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p>Fancy display heading</p>
                                <small class="text-muted">With faded secondary text</small>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</main>

<?php
get_footer();
